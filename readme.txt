In order for the wiki SDK to build with Linux machine,
there are a few steps of works that would require to
set it up correctly. You could also refer to the wiki
page for further details.

http://wiki.telink-semi.cn/wiki/IDE-and-Tools/Compiler_Linux/

For B91_ble_sdk, you could refer to TLSR921x-series chipset
wiki page [1].

For Windows IDE you would need it to generate the related
makefile and build configuration.	


To build with linux:

Toolchain download and setup:

	Download the toolchain from wiki: 
	0. mkdir;cd ~/toolchain
	1. wget http://wiki.telink-semi.cn/tools_and_sdk/Tools/IDE/telink_riscv_linux_toolchain.zip
    2. unzip telink_riscv_linux_toolchain.zip
    3. setup the toolchain path:
		user:/tmp/project$ export TELINK_TOOLCHAIN_PATH=$HOME/toolchain/telink_riscv_linux_toolchain
	4. setpu cross compile path:
		user:/tmp/project$ export CROSS_COMPILE=${TELINK_TOOLCHAIN_PATH}/nds32le-elf-mculib-v5f/bin/riscv32-elf-

Environment Setup and start to build:
	0. user:/tmp/project$ git clone git@gitlab.com:ryanhsutw/B91_ble_sdk.git
	1. setup the WORKSPACE for your build
		user:/tmp/project$ export WORKSPACE=`pwd`
    3. user:/tmp/project$ cd B91_ble_sdk
	4. user:/tmp/project$ cd ${PROJECT_FOLDER} # B91_ble_sampe, B91_module, B91_features
    5. user:/tmp/project$ make

To convert the makefile and mk files path info:

SDK from wiki will only be imported to Windows/Linux IDE,
and generate the makefile and all .mk file that path
information is incorrect for Linux build, you'll need to
convert that to linux path in order to build.

You'll need the an either Linux or Windows IDE setup in
order to generate the initial build for you. For the infromation
of IDE, you could refer to the wiki page [3].

Once you've downloaded the SDK, imported to IDE, and generate
the first build, you could copy the entire project to a
Linux machine or any tools that could handle the string replacement.

Here is one example of the command you could run in Linux or Cygwin
environment to do the string replacment for the makefile/mk files
path replacment.

To replacement the path, e.g

    1. windows path:    V:\telink\B91_ble_sdk
    2. linux path:      ${WORKSPACE}
	3. go into the ${PROJECT_FOLDER} and run below commands
		user:/tmp/project$ find -type f -name * -print0 | xargs -0 sed -i 's|Common|common|g'
		user:/tmp/project$ find . -name "*.mk" -print0 | xargs -0 sed -i 's|/cygdrive/C/Workspace/github|${WORKSPACE}|g'

[0] http://wiki.telink-semi.cn/wiki/IDE-and-Tools/Compiler_Linux/
[1] http://wiki.telink-semi.cn/wiki/chip-series/TLSR921x-Series/
[2] http://wiki.telink-semi.cn/wiki/IDE-and-Tools/RISC-V_IDE_for_TLSR9_Chips/
