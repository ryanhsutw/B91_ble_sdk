################################################################################
# Automatically-generated file. Do not edit!
################################################################################

OBJ_SRCS := 
S_SRCS := 
ASM_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
SAG_SRCS += \

SAG_FILE += \

GPROF_OUT := 
PWR_OUT := 
SIZE_OUTPUTS := 
READELF_OUTPUTS := 
EXECUTABLES := 
OBJDUMP_OUTPUTS := 
OBJS := 
SYMBOL_OUTPUTS := 
S_UPPER_DEPS := 
GCOV_OUT := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
3rd-party/freertos-V5 \
3rd-party/freertos-V5/portable/GCC/RISC-V \
3rd-party/freertos-V5/portable/MemMang \
application/app \
application/audio \
application/keyboard \
application/print \
application/usbstd \
boot/B91 \
common \
common/usb_dbg \
drivers/B91 \
drivers/B91/ext_driver \
drivers/B91/flash \
vendor/B91_feature \
vendor/B91_feature/feature_PHY_test \
vendor/B91_feature/feature_adv_power \
vendor/B91_feature/feature_audio \
vendor/B91_feature/feature_backup \
vendor/B91_feature/feature_conn_power \
vendor/B91_feature/feature_emi \
vendor/B91_feature/feature_extend_adv \
vendor/B91_feature/feature_gatt_security \
vendor/B91_feature/feature_ir \
vendor/B91_feature/feature_ll_state \
vendor/B91_feature/feature_multi_local_dev \
vendor/B91_feature/feature_ota \
vendor/B91_feature/feature_phy_conn \
vendor/B91_feature/feature_phy_extend_adv \
vendor/B91_feature/feature_slave_dle \
vendor/B91_feature/feature_smp_security \
vendor/B91_feature/feature_soft_timer \
vendor/B91_feature/feature_stuck_key \
vendor/B91_feature/feature_use_reg_buffer \
vendor/B91_feature/feature_whitelist \
vendor/common \

