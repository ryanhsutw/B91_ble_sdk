################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../common/plic_isr.c \
../common/utility.c 

OBJS += \
./common/plic_isr.o \
./common/utility.o 

C_DEPS += \
./common/plic_isr.d \
./common/utility.d 


# Each subdirectory must supply rules for building sources it contributes
common/%.o: ../common/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Andes C Compiler'
	$(CROSS_COMPILE)gcc -DCHIP_TYPE=CHIP_TYPE_9518 -D__PROJECT_B91_BLE_SAMPLE__=1 -I"${WORKSPACE}/B91_ble_sdk" -I"${WORKSPACE}/B91_ble_sdk/3rd-party/freertos-V5/portable/GCC/RISC-V" -I"${WORKSPACE}/B91_ble_sdk/3rd-party/freertos-V5/include" -I../drivers/B91 -I../vendor/common -I../common -O2 -mcmodel=medium -flto -g3 -Wall -mcpu=d25f -ffunction-sections -fdata-sections -c -fmessage-length=0 -fno-builtin -fomit-frame-pointer -fno-strict-aliasing -fshort-wchar -fuse-ld=bfd -fpack-struct -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d) $(@:%.o=%.o)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


